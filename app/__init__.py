#app/__init__.py

from flask_restplus import Api
from flask import Blueprint

# ejemplos tomados de UFENIX ---- Aqui hay que llamar a los controladores
from .main.person_controller import api as person_ns

blueprint = Blueprint('happyAPI', __name__, url_prefix='/happyAPI')

api = Api(blueprint,
          title='happyAPI!',
          version='1.0',
          description='''---
Aquí va la descripción/presentación de la API. Cambiar descripción en el documento __app/init.py__.

Bienvenido a la happyAPI!

Puedes acceder a la API por dos vias:
- Mediante esta misma página, clica primero sobre el endpoint (_person_), sobre _GET_, y luego sobre _Try it out_. Rellena los campos que desees siguiendo las indicaciones y clica el botón _Execute_. Te apareceran las respuestas bajo _Response body_.

- Añadiendo _person?_ y los filtros que desees a la dirección web en que te encuentras ahora mismo. [Más info](https://gitlab.com/cetaqua/happyapi/-/wikis/Home/uso)

Si tu consulta no incluye ningún filtro, se devolverá la lista completa de personas.
Si en un mismo filtro incluyes varias etiquetas, se devolverán las personas que cumplen cada una de ellas. Por ejemplo, si usas el filtro _countries=Francia,Italia_ la respuesta incluirá todas aquellas personas que hayan estado en ambos países, sin importar si han estado en otros más. Lo mismo para el campo _hobbies_ y _birth_.
De este modo si quieres saber quantas personas han estado tanto en Francia como en Italia (AND), incluye los dos países en el filtro. En cambio si tu objetivo es conocer quienes han estado en Francia o bien en Italia (OR), debes hacer dos consultas, una con cada país.

La API no distingue entre mayúsculas y minúsculas, por lo que no te has de preocupar por eso. Escribe las tildes.

No olvides consultar la [wiki](https://gitlab.com/cetaqua/happyapi/-/wikis/home) del proyecto, donde encontrarás mucha más información.



![Generic badge](https://img.shields.io/badge/version-1.0.0-blue.svg)

[![gitlab](https://img.shields.io/static/v1?label=->&message=View_API_code&color=brightgreen)](https://gitlab.com/cetaqua/happyapi)

'''
          )

# ejemplos tomados de UFENIX ---- añadir los namespaces
api.add_namespace(person_ns, path='/person')



