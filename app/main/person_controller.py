from flask import request
from flask_restplus import Resource
from flask_restplus import reqparse
from flask_restplus import cors
from flask import jsonify

from .person_util import personDto
from .person_service import get_all_person
from bson.json_util import dumps, loads

import json
import pandas

api = personDto.api
_personDto = personDto.person

parser = reqparse.RequestParser()
#parser.add_argument('name')
parser.add_argument('hobbies')
parser.add_argument('countries')
parser.add_argument('birth')

@api.route('')
@api.expect(parser, validate=True)
#@api.param('name', 'Nombre y apellido, separado por espacios')
@api.param('hobbies',    'Uno o más hobbies, separados por comas')
@api.param('countries',  'Uno o más paises visitados, separados por comas')
@api.param('birth', 	 'Mes o meses de nacimiento')


class personList(Resource):
	@cors.crossdomain(origin='*')
	@api.doc('description in person_controller')
	@api.doc(responses={200: 'Search results matching criteria'})
	@api.doc(responses={400: 'Bad input parameters'})
	@api.doc(responses={404: 'Not supported call'})
	@api.doc(responses={500: 'Internal server error'})
	def get(self):
		#Bloc comentari per afegir descripció a swagger ui
		args = parser.parse_args()
		print(args)

		all_person = get_all_person()
		data = dumps(all_person)
		llista = loads(data)
		df = pandas.DataFrame(llista).dropna()
		num_total = len(df)
		df['_id'] = df['_id'].astype(str)


		if args['hobbies']:
			aficiones = args['hobbies'].split(',')
			aficiones = [x.lower().lstrip() for x in aficiones]
			indexer = df['hobbies'].apply(lambda x: all([item in x for item in aficiones]))  # filtro AND
			#indexer = pandas.DataFrame(df['hobbies'].tolist()).isin(aficiones).all(1)       # filtro OR
			df = df[indexer]

		if args['countries'] and not df.empty:
			df.reset_index(inplace=True)
			paises = args['countries'].split(',')
			paises = [x.title().lstrip() for x in paises]
			indexer = df['countries'].apply(lambda x: all([item in x for item in paises]))  # filtro AND
			#indexer = pandas.DataFrame(df['hobbies'].tolist()).isin(aficiones).all(1)      # filtro OR
			df = df[indexer]
			df.drop(['index'], axis='columns', inplace=True)

		if args['birth'] and not df.empty:
			meses = args['birth'].split(',')
			meses = [x.lower() for x in meses]
			df = df[df['birth'].isin(meses)]


		print(len(df),'/',num_total)
		
		df.drop(['_id'], axis='columns', inplace=True)
		df.drop(['hobbies'], axis='columns', inplace=True)
		df.drop(['countries'], axis='columns', inplace=True)
		df.drop(['birth'], axis='columns', inplace=True)
		
		person_list = json.loads(df.to_json(orient='records'))
		try:
			del person_list['index']
		except:
			print('couldnt del person list index')

		return jsonify(person_list), 200
