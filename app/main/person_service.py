import uuid

from app.main.person_model import person
from app.main import db

from mongoalchemy.session import Session

def get_all_person():
    list_person_properties = []
    list = db.session.query(person).all()
    for item in list:
        list_person_properties.append(item.wrap())
    return list_person_properties
