from mongoalchemy.document import Document
from mongoalchemy.fields import StringField, IntField, ObjectId, DocumentField, ListField, FloatField


class person(Document):
    _id = ObjectId()
    name = StringField()
    hobbies = ListField(StringField())
    countries = ListField(StringField())
    birth = StringField()
