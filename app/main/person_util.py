from flask_restplus import Namespace, fields

class personDto:
    api = Namespace('person', description='') #afegir aquí descripció per a que es vegi al costat del endpoint en el swagger ui

    person = api.model('person', {
        'name': fields.String(description='name'),
        'hobbies': fields.List(fields.String(description='list of hobbies')),
        'countries': fields.List(fields.String(description='list of visited countries')),
        'birth': fields.String(description="month of birth")
    }) 
    
