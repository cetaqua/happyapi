import os

'''
Este archivo permite a la api acceder a la base de datos. Esta base de datos se
encuentra en una máquina virtual (un 'ordenador') distinto al que está 
ejecutando la API, por lo que hay que indicarle dónde se encuentra y cuáles son
las credenciales de acceso y contraseñas. Esto se hace en este archivo.

Ya que esto que ves es público, y cualquiera puede acceder, no es seguro
mostrar estos datos y por lo tanto se han eliminado. Por ello si descargas el
repositorio y lo corres en tu ordenador, la API no funcionará.

Se definen tres bases de datos distintas, y la API se conecta a una u otra en 
función del punto de desarrollo.
'''


basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', '')
    DEBUG = False


class DevelopmentConfig(Config):    
    # Dependiendo si usamos MongoDB o bbdd sql se usa una u otra
    #SQLALCHEMY_CONNECTION_STRING = 'sqlite:///' + os.path.join(basedir, 'flask_boilerplate_main.db')
    #SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    MONGOALCHEMY_OPTIONS ='authSource=admin'
    MONGOALCHEMY_SERVER_AUTH = False
    MONGOALCHEMY_SERVER =   ''
    MONGOALCHEMY_USER =     ''
    MONGOALCHEMY_PASSWORD = ''
    MONGOALCHEMY_DATABASE = ''


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    # Dependiendo si usamos MongoDB o bbdd sql se usa una u otra
    #SQLALCHEMY_CONNECTION_STRING = 'Depende del proyecto'
    MONGOALCHEMY_OPTIONS ='authSource=admin'
    MONGOALCHEMY_SERVER_AUTH = False
    MONGOALCHEMY_SERVER =   ''
    MONGOALCHEMY_USER =     ''
    MONGOALCHEMY_PASSWORD = ''
    MONGOALCHEMY_DATABASE = ''

class ProductionConfig(Config):
    DEBUG = False
    # Dependiendo si usamos MongoDB o bbdd sql se usa una u otra
    #SQLALCHEMY_CONNECTION_STRING = 'Depende del proyecto'
    MONGOALCHEMY_OPTIONS ='authSource=admin'
    MONGOALCHEMY_SERVER_AUTH = False
    MONGOALCHEMY_SERVER =   ''
    MONGOALCHEMY_USER =     ''
    MONGOALCHEMY_PASSWORD = ''
    MONGOALCHEMY_DATABASE = ''

config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY
