# HappyAPI

No olvides consultar la [wiki](https://gitlab.com/cetaqua/happyapi/-/wikis/home) 
del proyecto, donde encontrarás mucha información, sobre esta API y otros 
contenidos genéricos.

Si quieres saber más sobre el código, estás en el sitio correcto. Navega por las 
carpetas, en el espacio inferior encontrarás una descripcion sobre los archivos, 
archivos que también puedes clicar para visualizar.

En esta carpeta puedes encontrar:

### requirements.txt
Librerias necesarias para correr el código. Se incluye la versión de cada una de 
ellas para evitar incompatibilidades. 
Desde una terminal UNIX o conda instalar con el siguiente comando:

```
pip install -r requirements.txt
```

### manage.py
Se trata del script a ejecutar cuando se desea arrancar la API.
Da las siguientes órdenes:
* Ejectutar la creación de la app.
* Declarar "el lugar" dónde ubicar el servicio (ip y puerto)

Para ejecutar la API lanzar el siguiente comando desde una terminal UNIX, 
habiendo instalado las librerías de _requirements.txt_

